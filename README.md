# Splyt Full Stack Assignment

A map that shows the recent location of taxis around a given point of location. Made with React and Node.js in TypeScript. This was done as a Full Stack Interview Assignment for Splyt Technologies Ltd.

## List of Tools/Libraries/Frameworks used
- [Create React App](https://create-react-app.dev/)
- [Leaflet](https://leafletjs.com/)
- [React Leaflet](https://react-leaflet.js.org/)
- [React Toastify](https://fkhadra.github.io/react-toastify/introduction/)
- [Axios](https://github.com/axios/axios)
- [Fastify](https://www.fastify.io/)

## Running the Project
### Prerequisites
Download:
- [Node.js v14 LTS](https://nodejs.org/en/download/)

### Setup
```
# Clone repository
git clone git@gitlab.com:TheGreatGaben/splyt-assignment.git

# Go to project directory
cd splyt-assignment

# Install dependencies (for both app and backend)
yarn (or npm install)

# Copy environment variable file
cp .env.example .env
```
### Development
Set `ALLOWED_CORS_ORIGIN='*'` in `.env`, to enable CORS for the backend server. Then run:
#### `yarn dev` / `npm run dev`
This will run both app and backend server [concurrently](https://www.npmjs.com/package/concurrently) in development mode. The app can be accessed at `http://localhost:3000` in the browser. The backend server will be running at `http://localhost:{SERVER_PORT}`, where `SERVER_PORT` can be configured in `.env`.

### Production
Ensure to unset the variable `REACT_APP_API_BASE_URL` in `.env` before building, as it would not be needed anymore. The backend production server will serve the app's static build files directly.

#### `yarn build` / `npm run build`
Get production builds for both app and backend concurrently. The backend build will be output at `backend/dist`, while the app build will be output to `app/build`. The app production build would also be copied to `backend/public`.

#### `yarn start` / `npm run start`
Runs the backend server in production mode. Will serve the app as well from `backend/public` directory.
