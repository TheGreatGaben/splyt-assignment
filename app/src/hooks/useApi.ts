import { useState } from 'react'
import axios, { AxiosError, CancelTokenSource } from 'axios'

import { TaxiDriverData } from '../types'

interface GetTaxisConfig {
  headers?: object;
  params: {
    latitude: number;
    longitude: number;
    count: number;
  }
}

export interface TaxiApiResponse {
  data: {
    pickup_eta: number;
    drivers: TaxiDriverData[];
  }
}

export const useApi = () => {
  const [loading, setLoading] = useState(false)
  const [apiResponse, setApiResponse] = useState<TaxiApiResponse | null>(null)
  const [data, setData] = useState<TaxiDriverData[]>([])
  const [error, setError] = useState<AxiosError | null>(null)
  const [cancelSource, setCancelSource] = useState<CancelTokenSource | undefined>(undefined)

  const fetchData = async (config: GetTaxisConfig): Promise<void> => {
    if (cancelSource) {
      cancelSource.cancel()
    }
    const latestCancelSource = axios.CancelToken.source()
    setCancelSource(latestCancelSource)

    setLoading(true)
    try {
      const apiPath = `${process.env.REACT_APP_API_BASE_URL}${process.env.REACT_APP_TAXIS_API_PATH}`
      const axiosResponse = await axios.get(apiPath, {
        params: config.params,
        headers: config.headers,
        cancelToken: latestCancelSource.token
      })
      const response: TaxiApiResponse = axiosResponse.data
      setApiResponse(response)
      setData(response.data.drivers)

    } catch (error) {
      console.error(error)
      setError(error)
    }
    setLoading(false)
  }

  return [loading, apiResponse, data, error, fetchData] as const
}
