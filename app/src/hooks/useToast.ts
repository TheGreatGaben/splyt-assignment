import { toast } from 'react-toastify'

export const useToast = () => {
  const notifyTypes = {
    error(message: string) {
      toast.error(message)
    }
  }
  return [notifyTypes] as const
}
