import { getNearestLocation } from './common'
import { getMidpoint } from './math'

describe('common helper', () => {
  describe('getNearestLocation function', () => {
    const locations = [
      { lat: 51.5049375, lng: -0.0964509 },
      { lat: 1.285194, lng: 103.8522982 }
    ]

    test('should get the correct nearest location when given a reference location', () => {
      const reference = { lat: 3.1466, lng: 101.6958 }
      const result = getNearestLocation(reference, locations)
      expect(result).toEqual(locations[1])
    })
    test('should get the correct nearest location when given 0 coords', () => {
      const reference = { lat: 0, lng: 0 }
      const result = getNearestLocation(reference, locations)
      expect(result).toEqual(locations[0])
    })
    test('should get the correct nearest location when given negative coords', () => {
      const reference = { lat: -12312, lng: -1231 }
      const result = getNearestLocation(reference, locations)
      expect(result).toEqual(locations[1])
    })
    test('should get the correct nearest location when given a midpoint', () => {
      const reference = getMidpoint(locations[0], locations[1])
      const result = getNearestLocation(reference, locations)
      expect(result).toEqual(locations[0])
    })
  })
})
