import { LatLngCoords } from '../types'
import { euclidDistance, round } from './math'

export const getNearestLocation = (reference: LatLngCoords, locations: LatLngCoords[]): LatLngCoords => {
  const sortedLocations = [...locations].sort((locationA, locationB) => {
    const distA = round(euclidDistance(reference, locationA), 2)
    const distB = round(euclidDistance(reference, locationB), 2)
    return distA - distB
  })
  return sortedLocations[0]
}
