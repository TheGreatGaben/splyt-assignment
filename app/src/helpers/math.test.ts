import { euclidDistance, getMidpoint, round } from './math'

describe('math helper', () => {
  describe('euclidDistance function', () => {
    test('should correctly calculate the euclid distance between two coords', () => {
      const coordA = { lat: 51.5049375, lng: -0.0964509 }
      const coordB = { lat: 1.285194, lng: 103.8522982 }
      expect(euclidDistance(coordA, coordB)).toEqual(115.4442076358123)
    })
    test('should throw the expected error when given NaN coords values', () => {
      const coordA = { lat: NaN, lng: NaN }
      const coordB = { lat: NaN, lng: NaN }
      const expectedError = new Error('NaN values given')
      expect(() => euclidDistance(coordA, coordB)).toThrow(expectedError)
    })
  })

  describe('getMidpoint function', () => {
    test('should correctly get the midpoint between two coords', () => {
      const coordA = { lat: 51.5049375, lng: -0.0964509 }
      const coordB = { lat: 1.285194, lng: 103.8522982 }
      expect(getMidpoint(coordA, coordB)).toEqual({ lat: 26.395065749999997, lng: 51.87792365000001 })
    })
    test('should throw the expected error when given NaN coords values', () => {
      const coordA = { lat: NaN, lng: NaN }
      const coordB = { lat: NaN, lng: NaN }
      const expectedError = new Error('NaN values given')
      expect(() => getMidpoint(coordA, coordB)).toThrow(expectedError)
    })
  })

  describe('round function', () => {
    test('should round to the nearest whole number when decimalPlaces = 0', () => {
      expect(round(100.4)).toEqual(100)
      expect(round(100.5)).toEqual(101)
      expect(round(100.7)).toEqual(101)
    })
    test('should round to 2 d.p when decimalPlaces = 2', () => {
      expect(round(100.434, 2)).toEqual(100.43)
      expect(round(100.535, 2)).toEqual(100.54)
      expect(round(100.737, 2)).toEqual(100.74)
    })
    test('should round negative numbers correctly to 2 d.p when decimalPlaces = 2', () => {
      expect(round(-100.434, 2)).toEqual(-100.43)
      expect(round(-100.5351, 2)).toEqual(-100.54)
      expect(round(-100.737, 2)).toEqual(-100.74)
    })
    test('should throw the expected error when given NaN values', () => {
      const expectedError = new Error('NaN values given')
      expect(() => round(NaN, NaN)).toThrow(expectedError)
    })
    test('should throw the expected error when decimalPlaces < 0', () => {
      const expectedError = new Error('decimalPlaces cannot be negative')
      expect(() => round(123, -12321)).toThrow(expectedError)
    })
  })
})
