import { LatLngCoords } from '../types'

const validateIsNotNaN = (inputNums: number[]): void => {
  inputNums.forEach(num => {
    if (Number.isNaN(num)) {
      throw new Error('NaN values given')
    }
  })
}

export const euclidDistance = (locationA: LatLngCoords, locationB: LatLngCoords): number => {
  const flattenCoords = [locationA.lat, locationA.lng, locationB.lat, locationB.lng]
  validateIsNotNaN(flattenCoords)

  const diffX = locationA.lng - locationB.lng
  const diffY = locationA.lat - locationB.lat
  return Math.sqrt((diffX * diffX) + (diffY * diffY))
}

export const getMidpoint = (locationA: LatLngCoords, locationB: LatLngCoords): LatLngCoords => {
  const flattenCoords = [locationA.lat, locationA.lng, locationB.lat, locationB.lng]
  validateIsNotNaN(flattenCoords)

  const midLat = (locationA.lat + locationB.lat) / 2
  const midLng = (locationA.lng + locationB.lng) / 2
  return { lat: midLat, lng: midLng }
}

export const round = (num: number, decimalPlaces: number = 0): number => {
  validateIsNotNaN([num, decimalPlaces])
  if (decimalPlaces < 0) {
    throw new Error('decimalPlaces cannot be negative')
  }

  let exponent = 1
  for (let i = 0; i < decimalPlaces; i++) {
    exponent *= 10
  }
  // To deal with floating point precision errors, e.g: 1.005 * 100 = 100.4999999..
  const epsilon = num >= 0 ? Number.EPSILON : -Number.EPSILON
  return Math.round((num + epsilon) * exponent) / exponent
}
