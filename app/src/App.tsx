import React, { useEffect, useState } from 'react'
import './App.css'

import Map from './components/Map'
import TaxiMarker from './components/TaxiMarker'
import ControlWidget from './components/ControlWidget'
import ToastMessage from './components/ToastMessage'

import { Location } from './types'
import { getNearestLocation } from './helpers/common'
import { useApi } from './hooks/useApi'
import { useToast } from './hooks/useToast'

function App() {
  const defaultLocations: Location[] = [
    {
      id: 'london-splyt',
      name: 'London Splyt Office',
      coordinates: { lat: 51.5049375, lng: -0.0964509 },
    },
    {
      id: 'sg-splyt',
      name: 'Singapore Splyt Office',
      coordinates: { lat: 1.285194, lng: 103.8522982 },
    }
  ]
  const userLocationId = 'user-location'
  const refreshInterval = 10 // seconds
  const minTaxiCount = Number(process.env.REACT_APP_MIN_TAXI_COUNT) || 5
  const maxTaxiCount = Number(process.env.REACT_APP_MAX_TAXI_COUNT) || 100
  const toastMessageDuration = 5000

  const [init, setInit] = useState(false)
  const [toast] = useToast()

  const [locations, setLocations] = useState(defaultLocations)
  const [renderMap, setRenderMap] = useState(false)
  const [mapCenter, setMapCenter] = useState(defaultLocations[0].coordinates)

  const [toggleRefresh, setToggleRefresh] = useState(true)
  const [refreshCounter, setRefreshCounter] = useState(refreshInterval)
  const [refreshTimeout, setRefreshTimeout] = useState<NodeJS.Timeout | null>(null)

  const [taxiCount, setTaxiCount] = useState(minTaxiCount)
  const [apiLoading, apiResponse, taxiDrivers, apiError, fetchData] = useApi()

  useEffect(() => {
    setLocationNearestToUser()
  }, [])

  useEffect(() => {
    if (init) {
      callApi()
    }
  }, [mapCenter.lat, mapCenter.lng, init])

  useEffect(() => {
    if (apiError) {
      toast.error('Failed to fetch taxis. Try again later.')
    }
  }, [apiError])

  useEffect(() => {
    if (refreshTimeout) {
      clearTimeout(refreshTimeout)
    }

    if (toggleRefresh) {
      const timeout = setTimeout(async () => {
        const newCounter = refreshCounter - 1
        if (newCounter === 0) {
          await callApi()
        } else {
          setRefreshCounter(newCounter)
        }
      }, 1000)
      setRefreshTimeout(timeout)
    }
  }, [refreshCounter, toggleRefresh])

  const setLocationNearestToUser = () => {
    navigator.geolocation.getCurrentPosition(position => {
      const { latitude, longitude } = position.coords
      const currentLocation = { lat: latitude, lng: longitude }
      const locationCoords = defaultLocations.map(location => location.coordinates)
      const nearest = getNearestLocation(currentLocation, locationCoords)
      setMapCenter(nearest)
      setRenderMap(true)
      setLocations([
        {
          id: userLocationId,
          name: 'User Location',
          coordinates: currentLocation
        },
        ...defaultLocations
      ])
      setInit(true)
    }, error => {
      console.error(error)
      toast.error('Failed to get user location. Using a default location instead...')

      setMapCenter(defaultLocations[0].coordinates)
      setRenderMap(true)
      setInit(true)
    })
  }

  const callApi = async () => {
    await fetchData({
      params: {
        latitude: mapCenter.lat,
        longitude: mapCenter.lng,
        count: taxiCount
      }
    })
    // Reset refresh counter
    setRefreshCounter(refreshInterval)
  }

  const handleLocationChange = (newLocationId: string) => {
    const newLocation = locations.find(location => location.id === newLocationId)
    if (!newLocation) {
      console.error('Location not found!')
      return
    }

    if (newLocationId === userLocationId) {
      setLocationNearestToUser()
    } else {
      setMapCenter(newLocation.coordinates)
    }
  }

  return (
    <div className="App">
      <ToastMessage position="top-center" duration={toastMessageDuration} closeOnClick/>
      <ControlWidget
        locations={locations}
        userLocationId={userLocationId}
        refreshCounter={refreshCounter}
        taxiCount={taxiCount}
        minTaxiCount={minTaxiCount}
        maxTaxiCount={maxTaxiCount}
        toggleRefresh={toggleRefresh}
        apiLoading={apiLoading}
        onLocationChange={handleLocationChange}
        onTaxiCountChange={newTaxiCount => setTaxiCount(newTaxiCount)}
        onToggleRefresh={() => setToggleRefresh(!toggleRefresh)}
        onGetTaxisClick={() => callApi()}
      />
      {
        renderMap ?
        <Map center={mapCenter}>
          {
            taxiDrivers.map((driver, index) => <TaxiMarker key={index} driverData={driver}/>)
          }
        </Map>
        :
        <div>Loading Map...</div>
      }
    </div>
  );
}

export default App;
