export type LatLngCoords = {
  lat: number;
  lng: number;
}

export type Location = {
  id: string;
  name: string;
  coordinates: LatLngCoords;
}

export type TaxiDriverData = {
  driver_id: string;
  location: {
    latitude: number;
    longitude: number;
    bearing: number;
  }
}
