import React from 'react'
import './RangeSlider.css'

interface RangeSliderProps {
  min: number;
  max: number;
  step: number;
  defaultValue: number;
  disabled?: boolean;
  onChange(event: React.ChangeEvent<HTMLInputElement>): void;
}

export default function RangeSlider({ min, max, step, defaultValue, disabled = false, onChange }: RangeSliderProps) {
  return (
    <div className="rangeSliderContainer">
      <span>{min}</span>
      <input type="range" min={min} max={max} step={step} defaultValue={defaultValue} disabled={disabled} onChange={onChange}/>
      <span>{max}</span>
    </div>
  )
}
