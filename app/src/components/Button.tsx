import React, { ReactNode } from 'react'

interface ButtonProps {
  className: string;
  style?: React.CSSProperties;
  children: ReactNode | string;
  disabled?: boolean;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

export default function Button({ className, style, children, disabled = false, onClick }: ButtonProps) {
  return (
    <button className={className} style={style} type="button" onClick={onClick} disabled={disabled}>
      {children}
    </button>
  )
}
