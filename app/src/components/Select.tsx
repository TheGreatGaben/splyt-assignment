import React from 'react'

type SelectOption = {
  name: string;
  value: string;
}

interface SelectProps {
  options: SelectOption[];
  disabled?: boolean;
  onChange(event: React.ChangeEvent<HTMLSelectElement>): void;
}

export default function Select({ options, disabled = false, onChange }: SelectProps) {
  return (
    <select onChange={onChange} disabled={disabled}>
      {
        options.map((option, index) => <option key={index} value={option.value}>{option.name}</option>)
      }
    </select>
  )
}
