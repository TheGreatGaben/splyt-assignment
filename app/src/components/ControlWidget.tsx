import React from 'react'
import './ControlWidget.css'

import LocationSelect from './Select'
import TaxiNumberSlider from './RangeSlider'
import Button from './Button'

import { Location } from '../types'

interface ControlWidgetProps {
  locations: Location[];
  userLocationId: string;
  refreshCounter: number;
  taxiCount: number;
  minTaxiCount: number;
  maxTaxiCount: number;
  toggleRefresh: boolean;
  apiLoading: boolean;
  onLocationChange(newLocationId: string): void;
  onTaxiCountChange(newTaxiCount: number): void;
  onToggleRefresh(): void;
  onGetTaxisClick(): void;
}

export default function ControlWidget({
  locations, userLocationId, refreshCounter, taxiCount, minTaxiCount, maxTaxiCount, toggleRefresh, apiLoading,
  onLocationChange, onTaxiCountChange, onToggleRefresh, onGetTaxisClick
}: ControlWidgetProps) {
  const onSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    onLocationChange(event.target.selectedOptions[0].value)
  }

  const onSliderChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onTaxiCountChange(Number(event.target.value))
  }

  return (
    <div className="controlWidgetContainer">
      <div>
        <label>Location:</label><br/>
        <LocationSelect disabled={apiLoading || refreshCounter === 1} options={locations.map(location => {
            let name = location.name
            if (location.id === userLocationId) {
              name = 'Nearest to User Location'
            }
            return { name, value: location.id }
          })}
          onChange={onSelectChange}
        />
      </div>

      <div className="taxiNumberSliderContainer">
        <label>Number of Taxis: <b>{taxiCount}</b></label><br/>
        <TaxiNumberSlider
          min={minTaxiCount} max={maxTaxiCount} step={1} defaultValue={taxiCount}
          disabled={apiLoading || refreshCounter === 1}
          onChange={onSliderChange}
        />
      </div>

      <div>
        Refreshing taxi locations in {refreshCounter} seconds...
      </div>

      <Button className="blockBtn toggleRefreshBtn" onClick={() => onToggleRefresh()}>
        { toggleRefresh ? 'Stop Refresh' : 'Continue Refresh' }
      </Button>

      {
        apiLoading || refreshCounter === 1?
          <Button
            className="blockBtn loadingBtn"
            disabled={true}
          >
            { apiLoading ?  'Loading Taxis...' : 'Disabled' }
          </Button>
        :
          <Button
            className="blockBtn getTaxisBtn"
            onClick={() => onGetTaxisClick()}
          >
            Get Taxis Now!
          </Button>
      }
    </div>
  )
}
