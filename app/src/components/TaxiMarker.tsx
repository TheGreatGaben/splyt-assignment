import React from 'react'
import L, { Icon, LatLngExpression } from 'leaflet'
import { Marker, MarkerProps, Popup } from 'react-leaflet'

import { TaxiDriverData } from '../types'

import carIcon from '../assets/car-top-icon-63-32x32.png'

interface TaxiMarkerProps extends Omit<MarkerProps, 'position'> {
  driverData: TaxiDriverData;
  position?: LatLngExpression;
}

export default function TaxiMarker({ driverData, ...props }: TaxiMarkerProps) {
  const { latitude, longitude } = driverData.location
  const taxiPosition: LatLngExpression = [latitude, longitude]
  const taxiIcon: Icon = L.icon({
    iconUrl: carIcon,
    iconAnchor: [16, 16]
  })

  return (
    <Marker position={taxiPosition} icon={taxiIcon} {...props}>
      <Popup>
        <b>{driverData.driver_id}</b>
        <div><b>lat:</b> {driverData.location.latitude}</div>
        <div><b>lng:</b> {driverData.location.longitude}</div>
        <div><b>bearing:</b> {driverData.location.bearing}</div>
      </Popup>
    </Marker>
  )
}
