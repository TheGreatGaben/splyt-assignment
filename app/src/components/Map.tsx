import React, { useState, useEffect, ReactNode } from 'react'
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import L, { LatLngExpression, Icon, Map } from 'leaflet'
import './Map.css'
import 'leaflet/dist/leaflet.css'

import markerIcon from 'leaflet/dist/images/marker-icon.png'
import markerIconShadow from 'leaflet/dist/images/marker-shadow.png'

import { LatLngCoords } from '../types'

interface MapProps {
  center: LatLngCoords;
  children?: ReactNode;
}

export default function CustomMap({ center, children }: MapProps) {
  const centerCoords: LatLngExpression = [center.lat, center.lng]
  const [map, setMap] = useState<Map | null>(null)

  const DefaultIcon: Icon = L.icon({
    iconUrl: markerIcon,
    shadowUrl: markerIconShadow,
    iconAnchor: [13, 20],
  })

  useEffect(() => {
    if (map) {
      map.panTo(new L.LatLng(center.lat, center.lng))
    }
  }, [center.lat, center.lng])

  return (
    <div className="mapParentContainer">
      <MapContainer
        center={centerCoords}
        zoom={14}
        style={{ height: '100%' }}
        whenCreated={setMap}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={centerCoords} icon={DefaultIcon}>
          <Popup>
            You are here!
          </Popup>
        </Marker>

        {children}
      </MapContainer>
    </div>
  )
}
