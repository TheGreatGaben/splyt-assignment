import React, { ReactNode } from 'react'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.min.css'

interface ToastMessageProps {
  position: 'top-left' | 'top-right' | 'top-center' | 'bottom-left' | 'bottom-right' | 'bottom-center';
  duration: number;
  closeOnClick: boolean;
  children?: ReactNode;
}

export default function ToastMessage({ position, duration, closeOnClick, children }: ToastMessageProps) {
  return (
    <ToastContainer
      position={position} autoClose={duration} closeOnClick={closeOnClick} rtl={false}
      pauseOnHover={false} pauseOnFocusLoss={false}
    >
      {children}
    </ToastContainer>
  )
}
