import axios, { AxiosInstance, AxiosResponse, AxiosError } from 'axios'

export default class ApiClient {
  private baseUrl: string
  private httpClient: AxiosInstance

  constructor(url: string, headers: object = {}) {
    this.baseUrl = url
    this.httpClient = axios.create({
      baseURL: url,
      headers
    })
  }

  public async get(path: string, queryParams: object, headers: object = {}) {
    try {
      const response: AxiosResponse = await this.httpClient.get(path, {
        params: queryParams,
        headers
      })
      return response.data
    } catch (error) {
      this.logError(error)
      throw error
    }
  }

  private logError(error: AxiosError) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    } else if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      console.log(error.request);
    } else {
      // Something happened in setting up the request that triggered an Error
      console.log('Error', error.message);
    }
    console.log(error.config);
  }
}
