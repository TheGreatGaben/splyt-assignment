import Fastify, { FastifyInstance, FastifyServerOptions } from 'fastify'
import fastifyCors from 'fastify-cors'
import fastifyStatic from 'fastify-static'
import { routes } from './routes'
import path from 'path'

import dotenv from 'dotenv'
dotenv.config()

const options: FastifyServerOptions = {
  logger: process.env.ENABLE_LOGGER === 'true'
}
const server: FastifyInstance = Fastify(options)

server.register(fastifyCors, {
  origin: process.env.ALLOWED_CORS_ORIGIN || false
})
server.register(routes)
server.register(fastifyStatic, {
  root: path.join(__dirname, '../public')
})

const start = async () => {
  try {
    await server.listen(process.env.SERVER_PORT)

    const address = server.server.address()
    const port = typeof address === 'string' ? address : address?.port

  } catch (err) {
    server.log.error(err)
    process.exit(1)
  }
}
start()
