import { RouteShorthandOptions } from 'fastify'
import ApiClient from './helpers/apiClient'

const routeOptions: RouteShorthandOptions = {
  schema: {
    querystring: {
      type: 'object',
      properties: {
        latitude: {
          type: 'number'
        },
        longitude: {
          type: 'number'
        },
        count: {
          type: 'integer',
          minimum: 1
        }
      },
      required: ['latitude', 'longitude', 'count']
    }
  }
}

export async function routes(fastify, options) {
  fastify.get('/api/taxis', routeOptions, async (request, reply) => {
    const apiClient = new ApiClient(process.env.TAXIS_API_BASE_URL)
    try {
      const data = await apiClient.get(process.env.TAXIS_API_PATH, request.query)
      return { data }
    } catch (error) {
      reply.code(500).send(error)
    }
  })
}
